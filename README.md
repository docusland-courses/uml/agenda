# Exercice 1 :  Contacts

Nous désirons implanter la gestion des contacts:
- Cette application contient un ensemble de contacts de personnes.
- Chaque utilisateur disposera d'un compte
- Chaque compte sera lié à un ensemble de contacts
- Chaque contact est identifié par un nom et par un ensemble de coordonnées
- Une coordonnée peut être postale, téléphonique ou électronique (email ou page web)
- Une adresse email n’appartient qu’à une seule personne.
- Il est possible d'exporter une coordonnée.

## Attendus de l'exercice
Nous souhaitons réaliser une application simple permettant de faire un CRUD de base. ( Create, Read, Update, Delete). 

- Générer les uses cases implicites au sein de ce sujet.
- Générer le diagramme de classe (lié aux données) du sujet.

Attention, votre application de contacts peut être utilisé par plusieurs utilisateurs. Chaque utilisateur dispose de ses propre contacts.

---
# Exercice 2 :  Nouvelle contrainte
- Votre système fonctionne en système 3-tiers.
	- Vous avez un client mobile
	- Un serveur répondant aux appels API permettant de relayer les informations entre le client et la base de données
	- Une base de données
	
## Réaliser le diagramme de séquence 

Nous aimerions réaliser un diagramme de séquence entre deux clients simultanés, le serveur ainsi qu'avec la base de données.

Ainsi, deux clients :`Jake` et `Elwood` utilisant le même serveur simultanément. 
- L'un pour la création de la coordonnée postale d'une personne.
	- Utilisant la méthode `POST`
- L'autre réalisant un listing des personnes
	- En utilisant la méthode `GET`

## Réaliser un diagramme de temps

Nous aimerions réaliser un diagramme de temps pour la connection du client. Afin d'évaluer le temps qu'un utilisateur devrait attendre avant de visualiser un rendu sur son téléphone. 

Lorsque l'utilisateur charge l'accueil  de l'application client, un appel ajax est fait via une URL en HTTPS. L'URL devrait être résolue en une adresse IP. 

La résolution DNS peut ajouter un certain temps de latence perceptible par l'utilisateur. 

Le temps de latence lié à la résolution DNS peut être d'une milli seconde (DNS local) à quelques secondes (3 secondes maximum). 

Le serveur réceptionne l'appel avec une implémentation classique MVC (Modèle Vue Controlleur), une servlet java récupère le contrôle de l'appel et requête la base de données afin d'accéder à des modèles. 

Même sans intermédiaire par un DNS, la communication avec une base de données a tendance à être impactante en allocation de temps. Après avoir reçu et traité la donnée, la servlet transfère les données au client. 